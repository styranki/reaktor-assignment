use bad_api_types::{GameResult, Hand, Player};
use sqlx::{postgres::PgQueryResult, query};

#[derive(Eq, PartialEq)]
enum GameOutcome {
    Awin,
    Bwin,
    Tie,
}

/// Small utility function for computing a outcome of a game
fn game_result(game: &GameResult) -> GameOutcome {
    match (game.hand_a, game.hand_b) {
        (Hand::Scissors, Hand::Paper) => GameOutcome::Awin,
        (Hand::Rock, Hand::Paper) => GameOutcome::Bwin,
        (Hand::Paper, Hand::Scissors) => GameOutcome::Bwin,
        (Hand::Rock, Hand::Scissors) => GameOutcome::Awin,
        (Hand::Scissors, Hand::Rock) => GameOutcome::Bwin,
        (Hand::Paper, Hand::Rock) => GameOutcome::Awin,
        _ => GameOutcome::Tie,
    }
}

/// Convert hand enum to triple
/// (rock, paper, scissors)
fn hand_to_triple(hand: &Hand) -> (i32, i32, i32) {
    match hand {
        Hand::Rock => (1, 0, 0),
        Hand::Paper => (0, 1, 0),
        Hand::Scissors => (0, 0, 1),
    }
}

/// Adds a game and the players in it to db or upgrades the player rows accordingly
/// Fails if any of the insert / updates fails and rollbacks the transaction
pub async fn insert_game(
    game: GameResult,
    pool: sqlx::PgPool,
) -> Result<PgQueryResult, sqlx::Error> {
    if game.player_a == game.player_b {
        return Err(sqlx::Error::Protocol(
            "Can't play single player rps".to_string(),
        ));
    }
    let result = game_result(&game);
    let counts = match result {
        GameOutcome::Awin => (1, 0, 0),
        GameOutcome::Bwin => (0, 0, 1),
        GameOutcome::Tie => (0, 1, 0),
    };
    let hand_a = hand_to_triple(&game.hand_a);
    let mut transaction = pool.begin().await?;
    query!(
        r#"INSERT INTO players VALUES ($1, 1, $2, $3, $4, $5, $6, $7 )
                            ON CONFLICT ON CONSTRAINT players_pkey DO
                            UPDATE SET
                            number_of_games = players.number_of_games + 1,
                            number_of_wins = players.number_of_wins + $2,
                            number_of_ties = players.number_of_ties + $3,
                            number_of_losses = players.number_of_losses + $4,
                            number_of_rocks = players.number_of_rocks + $5,
                            number_of_papers = players.number_of_papers + $6,
                            number_of_scissors = players.number_of_scissors + $7
                         "#,
        game.player_a,
        counts.0,
        counts.1,
        counts.2,
        hand_a.0,
        hand_a.1,
        hand_a.2,
    )
    .execute(&mut transaction)
    .await?;

    let hand_b = hand_to_triple(&game.hand_b);
    query!(
        r#"INSERT INTO players VALUES ($1, 1, $2, $3, $4, $5, $6, $7 )
                            ON CONFLICT ON CONSTRAINT players_pkey DO
                            UPDATE SET
                            number_of_games = players.number_of_games + 1,
                            number_of_wins = players.number_of_wins + $2,
                            number_of_ties = players.number_of_ties + $3,
                            number_of_losses = players.number_of_losses + $4,
                            number_of_rocks = players.number_of_rocks + $5,
                            number_of_papers = players.number_of_papers + $6,
                            number_of_scissors = players.number_of_scissors + $7
                         "#,
        game.player_b,
        counts.2,
        counts.1,
        counts.0,
        hand_b.0,
        hand_b.1,
        hand_b.2,
    )
    .execute(&mut transaction)
    .await?;

    let game = query!(
        r#"INSERT INTO games VALUES ( $1, $2, $3, $4, $5, $6)"#,
        game.game_id,
        game.time as i64,
        game.player_a,
        game.player_b,
        game.hand_a as Hand,
        game.hand_b as Hand,
    )
    .execute(&mut transaction)
    .await?;

    transaction.commit().await?;
    Ok(game)
}

/// Gets player number of games, wins...
/// Query string player name
pub async fn get_player(player_name: String, pool: sqlx::PgPool) -> Result<Player, sqlx::Error> {
    sqlx::query_as!(Player, "SELECT * FROM players WHERE name = $1", player_name)
        .fetch_one(&pool)
        .await
}
/// Get games with a player has beent part of
/// Sorted by time
/// Pagination: default offset 0, limit 20
pub async fn get_player_games(
    player_name: String,
    offset: Option<u64>,
    limit: Option<u64>,
    pool: sqlx::PgPool,
) -> Result<Vec<GameResult>, sqlx::Error> {
    let games = sqlx::query_as!(GameResult, r#"
          SELECT game_id, time, player_a, player_b ,hand_a as "hand_a: _", hand_b as "hand_b:_" FROM games
          WHERE player_a =  $1 OR player_b = $1
          ORDER BY games.time DESC
          OFFSET $2
          LIMIT $3
        "#, player_name, offset.unwrap_or(0) as i64, limit.unwrap_or(20) as i64).fetch_all(&pool).await?;

    Ok(games)
}

pub async fn insert_visited_page(
    page: String,
    pool: sqlx::PgPool,
) -> Result<PgQueryResult, sqlx::Error> {
    sqlx::query!("INSERT INTO visited_pages VALUES ( $1 )", page)
        .execute(&pool)
        .await
}

/// Gets most recent games from the db (pagination) default limit 20
pub async fn get_most_recent_games(
    offset: Option<u64>,
    count: Option<u64>,
    pool: sqlx::PgPool,
) -> Result<Vec<GameResult>, sqlx::Error> {
    sqlx::query_as!(GameResult ,r#"
          SELECT game_id, time, player_a, player_b ,hand_a as "hand_a: _", hand_b as "hand_b:_" FROM games
          ORDER BY games.time DESC
          OFFSET $1
          LIMIT $2
        "#,
                                offset.unwrap_or(0) as i64,
                                count.unwrap_or(20) as i64,
    ).fetch_all(&pool)
     .await
}

/// Gets players from the db (pagination) default limit 20
pub async fn get_players(
    offset: Option<u64>,
    count: Option<u64>,
    pool: sqlx::PgPool,
) -> Result<Vec<Player>, sqlx::Error> {
    let players: Vec<Player> = sqlx::query_as!(
        Player,
        r#"
                        SELECT *
                        FROM players
                        ORDER BY name ASC
                        OFFSET $1
                        LIMIT $2
                     "#,
        offset.unwrap_or(0) as i64,
        count.unwrap_or(20) as i64,
    )
    .fetch_all(&pool)
    .await?;

    Ok(players)
}

pub async fn get_number_of_players(pool: sqlx::PgPool) -> Result<i64, sqlx::Error> {
    let result = sqlx::query!("SELECT COUNT(*) FROM players")
        .fetch_one(&pool)
        .await?;
    Ok(result.count.unwrap_or(0))
}
pub async fn get_number_of_games(pool: sqlx::PgPool) -> Result<i64, sqlx::Error> {
    let result = sqlx::query!("SELECT COUNT(*) FROM games")
        .fetch_one(&pool)
        .await?;
    Ok(result.count.unwrap_or(0))
}
