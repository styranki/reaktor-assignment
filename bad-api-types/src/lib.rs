/*! This library defines helper types that enable deserializing data from bad-api to Rust types.

   Optional feature flag "sqlx-pg" implements [`sqlx::Type`] for [`Hand`]. This feature is
   hidden behind feature flag to make it possible to use [`bad_api_types`] in wasm.

   Deserializer for [`Game`] had to be implemented manually because AFAIK [`serde`] doesn't support
   flattening of nested objects.
*/
use serde::{Deserialize, Serialize};

/// All possible hands / plays in rock paper scissors
#[derive(Debug, Deserialize, Serialize, Eq, PartialEq, Clone, Copy)]
#[cfg_attr(feature = "sqlx-pg", derive(sqlx::Type))]
#[cfg_attr(
    feature = "sqlx-pg",
    sqlx(type_name = "hand", rename_all = "UPPERCASE")
)]
pub enum Hand {
    #[serde(rename = "ROCK")]
    Rock,
    #[serde(rename = "PAPER")]
    Paper,
    #[serde(rename = "SCISSORS")]
    Scissors,
}

#[derive(Debug, Deserialize, Serialize)]
struct BadApiPlayer {
    name: String,
    #[serde(default)]
    #[serde(rename = "played")]
    hand: Option<Hand>,
}

#[derive(Debug, Deserialize, Serialize)]
struct BadApiGame {
    #[serde(rename = "type")]
    game_type: String,
    #[serde(rename = "gameId")]
    game_id: String,
    #[serde(default)]
    #[serde(rename = "t")]
    time: i64,
    #[serde(rename = "playerA")]
    player_a: BadApiPlayer,
    #[serde(rename = "playerB")]
    player_b: BadApiPlayer,
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Player {
    pub name: String,
    pub number_of_games: i32,
    pub number_of_wins: i32,
    pub number_of_ties: i32,
    pub number_of_losses: i32,
    pub number_of_rocks: i32,
    pub number_of_papers: i32,
    pub number_of_scissors: i32,
}

impl Player {
    pub fn most_played_hand(&self) -> (Hand, i32) {
        let counts: [i32; 3] = [
            self.number_of_rocks,
            self.number_of_papers,
            self.number_of_scissors
        ];

        // Iterate over counts to find index and number of plays for the most player hand
        // Maybe consider using "simpler" if else logic
        // Unwrap is safe here: max_by returns None only in case of empty list
        let index = counts.iter().enumerate().max_by(|(_, val_a), (_, val_b)| val_a.cmp(val_b)).unwrap();

        match index.0 {
            0 => (Hand::Rock, *index.1),
            1 => (Hand::Paper, *index.1),
            2 => (Hand::Scissors, *index.1),
            _ => unreachable!("Index greater than 2. Shoudn't be possible in a array of size 3.")
        }
    }
}

/// Two types of games: in-progress and finished
#[derive(Clone, Debug, PartialEq, Serialize)]
pub enum Game {
    Begin(GameBegin),
    Result(GameResult),
}
impl Game {
    pub fn game_id(&self) -> &str {
        match self {
            Game::Begin(s) => &s.game_id,
            Game::Result(s) => &s.game_id,
        }
    }
}

/// Rock paper scissors game in progress
#[derive(Clone, Debug, PartialEq, Serialize)]
pub struct GameBegin {
    #[serde(rename = "gameId")]
    pub game_id: String,
    pub player_a: String,
    pub player_b: String,
}
/// Result of a rock paper scissors game
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct GameResult {
    #[serde(rename = "gameId")]
    pub game_id: String,
    /// Unix timestamp in milliseconds
    #[serde(rename = "t")]
    pub time: i64,
    pub player_a: String,
    pub player_b: String,
    pub hand_a: Hand,
    pub hand_b: Hand,
}

impl From<BadApiGame> for Game {
    fn from(bad_api_game: BadApiGame) -> Self {
        match bad_api_game.game_type.as_str() {
            "GAME_RESULT" => Self::Result(GameResult {
                game_id: bad_api_game.game_id,
                time: bad_api_game.time,
                player_a: bad_api_game.player_a.name,
                player_b: bad_api_game.player_b.name,
                hand_a: bad_api_game.player_a.hand.unwrap(),
                hand_b: bad_api_game.player_b.hand.unwrap(),
            }),
            _ => Self::Begin(GameBegin {
                game_id: bad_api_game.game_id,
                player_a: bad_api_game.player_a.name,
                player_b: bad_api_game.player_b.name,
            }),
        }
    }
}

impl<'de> Deserialize<'de> for Game {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let bad_api_game = BadApiGame::deserialize(deserializer)?;
        if bad_api_game.game_type == "GAME_RESULT"
            && (bad_api_game.player_a.hand.is_none() || bad_api_game.player_b.hand.is_none())
        {
            return Err(serde::de::Error::custom(
                "Both players must play something. hand != None",
            ));
        }
        if !(bad_api_game.game_type == "GAME_RESULT" || bad_api_game.game_type == "GAME_BEGIN") {
            return Err(serde::de::Error::custom(format!(
                "Invalid game type: {}",
                bad_api_game.game_type
            )));
        }

        Ok(bad_api_game.into())
    }
}


/// Clean unwanted characters from websocket strings
pub fn clean_ws_string(s: String) -> String {
    // Delete backslashes
    let res: String = s.chars().filter(|&c| c != '\\').collect();
    // Trim start "
    let res: String = res.trim_end_matches('\"').to_string();
    // Trim end "
    let res: String = res.trim_start_matches('\"').to_string();

    res
}



#[cfg(test)]
mod tests {
    use super::*;
    use serde_json::from_str;

    #[test]
    fn test_deserialize() {
        let data = r#"{"type":"GAME_RESULT","gameId":"af14eee01dd84de4c3b612a","t":1641751927688,"playerA":{"name":"Ahti Mäkelä","played":"ROCK"},"playerB":{"name":"Kyllikki Mäkinen","played":"ROCK"}}"#;
        let game = Game::Result(GameResult {
            game_id: String::from("af14eee01dd84de4c3b612a"),
            time: 1641751927688,
            player_a: String::from("Ahti Mäkelä"),
            hand_a: Hand::Rock,
            player_b: String::from("Kyllikki Mäkinen"),
            hand_b: Hand::Rock,
        });

        assert_eq!(serde_json::from_str::<Game>(data).unwrap(), game);
    }

    #[test]
    fn test_deserialize_played_missing() {
        let data = r#"{"type":"GAME_RESULT","gameId":"af14eee01dd84de4c3b612a","t":1641751927688,"playerA":{"name":"Ahti Mäkelä","played":""},"playerB":{"name":"Kyllikki Mäkinen","played":"ROCK"}}"#;
        assert!(serde_json::from_str::<Game>(data).is_err());
    }

    #[test]
    fn test_deserialize_both_played_missing() {
        let data = r#"{"type":"GAME_RESULT","gameId":"af14eee01dd84de4c3b612a","t":1641751927688,"playerA":{"name":"Ahti Mäkelä","played":""},"playerB":{"name":"Kyllikki Mäkinen","played":""}}"#;
        assert!(serde_json::from_str::<Game>(data).is_err());
    }

    #[test]
    fn test_deserialize_unknown_type() {
        let data = r#"{"type":"GAME_KYLLIKKI","gameId":"af14eee01dd84de4c3b612a","t":1641751927688,"playerA":{"name":"Ahti Mäkelä","played":"ROCK"},"playerB":{"name":"Kyllikki Mäkinen","played":"ROCK"}}"#;

        assert!(serde_json::from_str::<Game>(data).is_err());
    }
}
