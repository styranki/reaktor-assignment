use yew::html::Scope;
use yew::prelude::*;
use yew_router::prelude::*;

mod components;
mod error;
mod pages;
mod services;

use pages::{
    game_list::GameList, home::Home, page_not_found::PageNotFound, player_page::PlayerPage,
    players::Players,
};

use components::current_games::CurrentGames;

#[derive(Routable, PartialEq, Clone, Debug)]
pub enum Route {
    #[at("/player/:name")]
    Player { name: String },
    #[at("/players")]
    Players,
    #[at("/games")]
    Games,
    #[at("/")]
    Home,
    #[not_found]
    #[at("/404")]
    NotFound,
}

fn switch(routes: &Route) -> Html {
    match routes.clone() {
        Route::Home => {
            html! {<Home />}
        }
        Route::Games => {
            html! { <GameList /> }
        }
        Route::Player { name } => {
            html! { <PlayerPage /> }
        }
        Route::Players => {
            html! { <Players /> }
        }
        _ => {
            html! { <PageNotFound /> }
        }
    }
}
pub enum Msg {
    ToggleNavbar,
}

pub struct App {
    navbar_active: bool,
}
impl Component for App {
    type Message = Msg;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self {
            navbar_active: false,
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::ToggleNavbar => {
                self.navbar_active = !self.navbar_active;
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        html! {
                <BrowserRouter>
                { self.view_nav(ctx.link()) }
                <main>
                <div class="columns">
                    <div class="column is-three-quarters">
                        <Switch<Route> render={Switch::render(switch)} />
                    </div>
                    <div class="column">
                        <CurrentGames />
                    </div>
                </div>
                </main>
                <footer class="footer">
                    <div class="content has-text-centered">
                        { "Powered by " }
                        <a href="https://yew.rs">{ "Yew" }</a>
                        { " using " }
                        <a href="https://bulma.io">{ "Bulma" }</a>
                    </div>
                </footer>
                </BrowserRouter>
        }
    }
}

impl App {
    fn view_nav(&self, link: &Scope<Self>) -> Html {
        let Self { navbar_active, .. } = *self;

        let active_class = if !navbar_active { "is-active" } else { "" };

        html! {
            <nav class="navbar is-dark" role="navigation" aria-label="main navigation">
                <div class="navbar-brand">
                    <h1 class="navbar-item is-size-3">{ "RPS Live" }</h1>
                    <button class={classes!("navbar-burger", "burger", active_class)}
                        aria-label="menu" aria-expanded="false"
                        onclick={link.callback(|_| Msg::ToggleNavbar)}
                    >
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                    </button>
                </div>
                <div class={classes!("navbar-menu", active_class)}>
                    <div class="navbar-start">
                        <Link<Route> classes={classes!("navbar-item")} to={Route::Home}>
                            { "Home" }
                        </Link<Route>>
                        <Link<Route> classes={classes!("navbar-item")} to={Route::Games}>
                            { "Games" }
                        </Link<Route>>
                        <Link<Route> classes={classes!("navbar-item")} to={Route::Players}>
                            { "Players" }
                        </Link<Route>>
                    </div>
                </div>
            </nav>
        }
    }
}

fn main() {
    wasm_logger::init(wasm_logger::Config::default());
    yew::start_app::<App>();
}
