use crate::components::{game_card::GameCard, player_card::PlayerCard};
use crate::services::{get_player, get_player_games, FetchState};
use bad_api_types::{Game, GameResult, Player};
use yew::prelude::*;
use yew_router::prelude::*;

#[derive(Clone, Debug, PartialEq, Properties)]
pub struct Props {
    pub name: String,
}
pub enum Message {
    GetPlayer,
    GetPlayerGames(String),
    UpdatePlayerData(FetchState<Player>),
    UpdateGameData(FetchState<Vec<GameResult>>),
}
pub struct PlayerPage {
    player: Option<Player>,
    player_fetch_state: FetchState<Player>,
    games: Vec<GameResult>,
    games_fetch_state: FetchState<Vec<GameResult>>,
    _listener: LocationHandle,
}

fn current_page(ctx: &Context<PlayerPage>) -> String {
    let location = ctx.link().location().unwrap();
    log::info!("{:?}", location);
    location.path().to_string()
}

impl Component for PlayerPage {
    type Message = Message;
    type Properties = ();

    fn create(ctx: &Context<Self>) -> Self {
        let link = ctx.link().clone();
        let listener = ctx
            .link()
            .add_location_listener(link.callback(move |_| Message::GetPlayer))
            .unwrap();
        ctx.link().send_message(Message::GetPlayer);

        Self {
            player: None,
            player_fetch_state: FetchState::NotFetching,
            games: Vec::new(),
            games_fetch_state: FetchState::NotFetching,
            _listener: listener,
        }
    }
    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Message::GetPlayer => {
                let name = current_page(ctx);
                // Check that has to be added because location link sends last message when exiting the page
                if name.starts_with("/player") && name != "/players" {
                    ctx.link().send_future(async move {
                        let request =
                            get_player(name.trim_start_matches("/player/").to_string()).await;
                        match request {
                            Ok(data) => Message::UpdatePlayerData(FetchState::Success(data)),
                            Err(e) => {
                                log::error! {"{:?}", e};
                                Message::UpdatePlayerData(FetchState::Failed)
                            }
                        }
                    });
                }
                ctx.link()
                    .send_message(Message::UpdatePlayerData(FetchState::Fetching));
                true
            }
            Message::UpdatePlayerData(fs) => {
                match fs {
                    FetchState::Success(data) => {
                        let name = data.name.clone();
                        ctx.link().send_message(Message::GetPlayerGames(name));
                        self.player = Some(data);
                        self.player_fetch_state = FetchState::NotFetching;
                    }
                    _ => {
                        self.player_fetch_state = fs;
                    }
                }
                true
            }
            Message::GetPlayerGames(name) => {
                ctx.link().send_future(async move {
                    let request =
                        get_player_games(name.trim_start_matches("/player/").to_string(), 10, 0)
                            .await;
                    match request {
                        Ok(data) => Message::UpdateGameData(FetchState::Success(data)),
                        Err(e) => {
                            log::error! {"{:?}", e};
                            Message::UpdateGameData(FetchState::Failed)
                        }
                    }
                });
                ctx.link()
                    .send_message(Message::UpdateGameData(FetchState::Fetching));
                true
            }
            Message::UpdateGameData(fs) => {
                match fs {
                    FetchState::Success(data) => {
                        self.games = data;
                        self.games_fetch_state = FetchState::NotFetching;
                    }
                    _ => {
                        self.games_fetch_state = fs;
                    }
                }
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        html! {
            <div class="section container">
                {self.player_fetch_state.view()}
                if let Some(p) = &self.player {
                    <PlayerCard player={p.clone()} />
                    <div class="section container">
                        <h2 class="title is-3"> {format!("{}'s recent games", self.player.as_ref().unwrap().name)} </h2>
                        {self.games_fetch_state.view()}
                        {for self.games.iter().map(|g| {
                            html!{<GameCard game={Game::Result(g.clone())} />}
                        })}
                    </div>
                }
            </div>
        }
    }
}
