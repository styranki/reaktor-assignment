use yew::prelude::*;

pub struct Home;
impl Component for Home {
    type Message = ();
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        html! {
                    <div class="tile is-ancestor is-vertical">
                        <div class="tile is-child hero">
                            <div class="hero-body container pb-0">
                                <h1 class="title is-1">{ "RPS live" }</h1>
                            </div>
                        </div>

                        <div class="tile is-child">
                            <figure class="image is-3by1">
                                <img alt="A random image for the input term 'rock'." src="https://source.unsplash.com/random/1200x400/?rock" />
                            </figure>
                        </div>

                        <div class="tile is-child hero">
                            <div class="hero-body container">
                                <h2 class="title is-3">{ "Follow what's happening in the world of RPS today" }</h2>
                                                    </div>
                        </div>
        <div class="tile is-parent container">
                                    { self.view_info_tiles() }
                                </div>

                    </div>
                }
    }
}

impl Home {
    fn view_info_tiles(&self) -> Html {
        html! {
            <>
                <div class="tile is-parent">
                    <div class="tile is-child box">
                        <p class="title">{ "What is RPS?" }</p>
                        <p class="subtitle">{ "Everything you need to know!" }</p>

                        <div class="content">
                            { r#"Rock paper scissors (also known by other orderings of the three items, with "rock" sometimes being called "stone",
                                or as Rochambeau, roshambo, or ro-sham-bo) is a hand game, usually played between two people, in which each
                                player simultaneously forms one of three shapes with an outstretched hand.
                            "#}
                            <br />
                            { r#"
                                -- https://en.wikipedia.org/wiki/Rock_paper_scissors
                            "# }
                        </div>
                    </div>
                </div>

                <div class="tile is-parent">
                    <div class="tile is-child box">
                        <p class="title">{ "What do we offer?" }</p>

                        <div class="content">
                <ul>
                    <li> {"Watch live games"} </li>
                    <li> {"Explore player statistics and recent games"} </li>
                    <li> {"See match history of all official rps games"} </li>
                </ul>
                        </div>
                    </div>
                </div>
            </>
        }
    }
}
