use crate::components::pagination::PageQuery;
use crate::components::{pagination::Pagination, player_card::PlayerCard};
use crate::services::{get_number_of_players, get_players, FetchState};
use crate::Route;

use bad_api_types::Player;
use yew::prelude::*;
use yew_router::prelude::*;

const PLAYERS_PER_PAGE: u64 = 10;

pub enum Msg {
    GetNumberOfPlayers,
    GetPlayers,
    SetNumberOfPlayers(FetchState<u64>),
    SetPlayers(FetchState<Vec<Player>>),
    PageUpdated,
}

pub struct Players {
    number_of_players: Option<u64>,
    fetch_state_number: FetchState<u64>,
    fetch_state_players: FetchState<Vec<Player>>,
    players: Vec<Player>,
    _listener: LocationHandle,
}

fn current_page(ctx: &Context<Players>) -> u64 {
    let location = ctx.link().location().unwrap();
    location.query::<PageQuery>().map(|it| it.page).unwrap_or(1)
}

impl Component for Players {
    type Message = Msg;
    type Properties = ();

    fn create(ctx: &Context<Self>) -> Self {
        let link = ctx.link().clone();
        let listener = ctx
            .link()
            .add_location_listener(link.callback(move |_| Msg::PageUpdated))
            .unwrap();
        ctx.link().send_message(Msg::GetNumberOfPlayers);
        Self {
            number_of_players: None,
            fetch_state_number: FetchState::NotFetching,
            players: Vec::new(),
            fetch_state_players: FetchState::NotFetching,
            _listener: listener,
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::GetNumberOfPlayers => {
                ctx.link().send_future(async move {
                    match get_number_of_players().await {
                        Ok(num) => Msg::SetNumberOfPlayers(FetchState::Success(num)),
                        Err(err) => {
                            log::error!("{}", err);
                            Msg::SetNumberOfPlayers(FetchState::Failed)
                        }
                    }
                });
                ctx.link()
                    .send_message(Msg::SetNumberOfPlayers(FetchState::Fetching));
                true
            }
            Msg::SetNumberOfPlayers(fs) => {
                match fs {
                    FetchState::Success(data) => {
                        self.number_of_players = Some(data);
                        self.fetch_state_number = FetchState::NotFetching;
                        ctx.link().send_message(Msg::GetPlayers);
                    }
                    _ => {
                        self.fetch_state_number = fs;
                    }
                }
                true
            }
            Msg::GetPlayers => {
                let page = current_page(ctx);
                ctx.link().send_future(async move {
                    match get_players(
                        PLAYERS_PER_PAGE,
                        (page.saturating_sub(1)) * PLAYERS_PER_PAGE,
                    )
                    .await
                    {
                        Ok(players) => Msg::SetPlayers(FetchState::Success(players)),
                        Err(err) => {
                            log::error!("{}", err);
                            Msg::SetPlayers(FetchState::Failed)
                        }
                    }
                });
                ctx.link()
                    .send_message(Msg::SetPlayers(FetchState::Fetching));
                true
            }
            Msg::SetPlayers(fs) => {
                match fs {
                    FetchState::Success(data) => {
                        self.players = data;
                        self.fetch_state_players = FetchState::NotFetching;
                    }
                    _ => {
                        self.fetch_state_players = fs;
                    }
                }
                true
            }
            Msg::PageUpdated => {
                ctx.link().send_message(Msg::GetPlayers);
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let page = current_page(ctx);

        html! {
            <div class="section container">
                <h1 class="title">{ "Players" }</h1>
                <h2 class="subtitle">{ "All players in alphabetical order" }</h2>
            {self.fetch_state_number.view()}
            if let Some(n) = self.number_of_players {
                <Pagination
                    {page}
                    total_pages={ (n.saturating_sub(1)) /PLAYERS_PER_PAGE + 1}
                    route_to_page={Route::Players}
                 />
            }
            {self.fetch_state_players.view()}
            <ul>
            {for self.players.iter().map(|g| {

                html!{ <PlayerCard player={g.clone()} /> }
            })
            }
            </ul>

            </div>
        }
    }
}
