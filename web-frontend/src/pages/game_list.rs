use crate::components::pagination::PageQuery;
use crate::components::{game_card::GameCard, pagination::Pagination};
use crate::services::{get_games, get_number_of_games, FetchState};
use crate::Route;

use bad_api_types::Game;
use yew::prelude::*;
use yew_router::prelude::*;

const GAMES_PER_PAGE: u64 = 10;

pub enum Msg {
    GetNumberOfGames,
    GetGames,
    SetNumberOfGames(FetchState<u64>),
    SetGameListState(FetchState<Vec<Game>>),
    PageUpdated,
}
pub struct GameList {
    number_of_games: Option<u64>,
    fetch_state_number: FetchState<u64>,
    fetch_state_games: FetchState<Vec<Game>>,
    games: Vec<Game>,
    _listener: LocationHandle,
}

fn current_page(ctx: &Context<GameList>) -> u64 {
    let location = ctx.link().location().unwrap();
    location.query::<PageQuery>().map(|it| it.page).unwrap_or(1)
}

impl Component for GameList {
    type Message = Msg;
    type Properties = ();

    fn create(ctx: &Context<Self>) -> Self {
        let link = ctx.link().clone();
        let listener = ctx
            .link()
            .add_location_listener(link.callback(move |_| Msg::PageUpdated))
            .unwrap();
        ctx.link().send_message(Msg::GetNumberOfGames);
        Self {
            number_of_games: None,
            games: Vec::new(),
            fetch_state_number: FetchState::NotFetching,
            fetch_state_games: FetchState::NotFetching,
            _listener: listener,
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::GetNumberOfGames => {
                ctx.link().send_future(async move {
                    match get_number_of_games().await {
                        Ok(num) => Msg::SetNumberOfGames(FetchState::Success(num)),
                        Err(err) => {
                            log::error!("{}", err);
                            Msg::SetNumberOfGames(FetchState::Failed)
                        }
                    }
                });
                ctx.link()
                    .send_message(Msg::SetNumberOfGames(FetchState::Fetching));
                true
            }
            Msg::SetNumberOfGames(fs) => {
                match fs {
                    FetchState::Success(data) => {
                        self.number_of_games = Some(data);
                        self.fetch_state_number = FetchState::NotFetching;
                        ctx.link().send_message(Msg::GetGames);
                    }
                    _ => {
                        self.fetch_state_number = fs;
                    }
                }
                true
            }
            Msg::GetGames => {
                let page = current_page(ctx);
                ctx.link().send_future(async move {
                    match get_games(GAMES_PER_PAGE, (page.saturating_sub(1)) * GAMES_PER_PAGE).await
                    {
                        Ok(mut games) => {
                            let tmp = games.drain(..).map(|g| Game::Result(g)).collect();
                            Msg::SetGameListState(FetchState::Success(tmp))
                        }
                        Err(err) => {
                            log::error!("{}", err);
                            Msg::SetGameListState(FetchState::Failed)
                        }
                    }
                });
                ctx.link()
                    .send_message(Msg::SetGameListState(FetchState::Fetching));
                true
            }
            Msg::SetGameListState(fs) => {
                match fs {
                    FetchState::Success(data) => {
                        self.games = data;
                        self.fetch_state_games = FetchState::NotFetching;
                    }
                    _ => {
                        self.fetch_state_games = fs;
                    }
                }
                true
            }
            Msg::PageUpdated => {
                //self.page = current_page(&ctx.clone());
                ctx.link().send_message(Msg::GetGames);
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let page = current_page(ctx);

        html! {
            <div class="section container">
                <h1 class="title">{ "Games" }</h1>
                <h2 class="subtitle">{ "Most recent games are listed first" }</h2>
            {self.fetch_state_number.view()}
            if let Some(n) = self.number_of_games {
                <Pagination
                    {page}
                    total_pages={ n/GAMES_PER_PAGE + 1}
                    route_to_page={Route::Games}
                    />
            }
            {self.fetch_state_number.view()}
            {for self.games.iter().map(|g| {
                html!{ <GameCard game={g.clone()} />}
            })
            }

            </div>
        }
    }
}
