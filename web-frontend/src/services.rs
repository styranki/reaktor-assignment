use crate::error::Error;
use bad_api_types::{GameResult, Player};
use dotenv_codegen::dotenv;
use serde::de::DeserializeOwned;
use yew::prelude::*;

const BASE_URL: &str = dotenv!("API_ROOT");

pub enum FetchState<T> {
    Fetching,
    Failed,
    Success(T),
    NotFetching,
}

impl<T> FetchState<T> {
    pub fn view(&self) -> Html {
        match self {
            FetchState::Success(_) => {
                html! {"Success"}
            }
            FetchState::Fetching => {
                html! {"Fetching"}
            }
            FetchState::Failed => {
                html! {"Failed"}
            }
            _ => {
                html! {""}
            }
        }
    }
}

pub async fn get_number_of_games() -> Result<u64, Error> {
    get(format!("{}{}", BASE_URL, "/games/count").to_string()).await
}
pub async fn get_number_of_players() -> Result<u64, Error> {
    get(format!("{}{}", BASE_URL, "/players/count").to_string()).await
}

pub async fn get_games(limit: u64, offset: u64) -> Result<Vec<GameResult>, Error> {
    get(format!(
        "{}{}",
        BASE_URL,
        format!("/games/recent?limit={}&offset={}", limit, offset)
    )
    .to_string())
    .await
}

pub async fn get_players(limit: u64, offset: u64) -> Result<Vec<Player>, Error> {
    get(format!(
        "{}{}",
        BASE_URL,
        format!("/players?limit={}&offset={}", limit, offset)
    )
    .to_string())
    .await
}
pub async fn get_player(name: String) -> Result<Player, Error> {
    get(format!("{}{}", BASE_URL, format!("/player/{}", name))).await
}
pub async fn get_player_games(
    name: String,
    limit: u64,
    offset: u64,
) -> Result<Vec<GameResult>, Error> {
    get(format!(
        "{}{}",
        BASE_URL,
        format!("/player/{}/games?limit={}&offset={}", name, limit, offset)
    ))
    .await
}

pub async fn get<T>(url: String) -> Result<T, Error>
where
    T: DeserializeOwned + 'static + std::fmt::Debug,
{
    let response = reqwest::get(url).await;
    if let Ok(data) = response {
        log::debug!("Response: {:?}", data);
        if data.status().is_success() {
            let data = data.json::<T>().await;
            if let Ok(data) = data {
                Ok(data)
            } else {
                Err(Error::DeserializeError)
            }
        } else {
            match data.status().as_u16() {
                401 => Err(Error::Unauthorized),
                403 => Err(Error::Forbidden),
                404 => Err(Error::NotFound),
                // Just leave the rest unhandled for now
                _ => Err(Error::RequestError),
            }
        }
    } else {
        Err(Error::RequestError)
    }
}
