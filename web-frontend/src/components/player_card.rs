use crate::Route;
use bad_api_types::{Game, Player, Hand};
use yew::prelude::*;
use yew_router::components::Link;

#[derive(Clone, Debug, PartialEq, Properties)]
pub struct Props {
    pub player: Player,
}
pub struct PlayerCard;

impl Component for PlayerCard {
    type Message = ();
    type Properties = Props;

    fn create(ctx: &Context<Self>) -> Self {
        Self
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let player = &ctx.props().player;
        let most_played_hand = player.most_played_hand();
        html! {
            <div class="card player">
                <div class="card-content">
                    <Link<Route> to={Route::Player {name: player.name.clone()}}>
                        <b> { format!("{}", player.name) } </b>
                    </Link<Route>>
                    <p>
                        { format!("Win rate: {:.2}%", player.number_of_wins as f32 / player.number_of_games as f32 * 100.0) }
                        <br />
                        { format!("Favourite hand {:?}: played {} times.", most_played_hand.0, most_played_hand.1) }
                        <br />
                        { format!("Games: {}", player.number_of_games) }
                        <br />
                        { format!("Wins: {}", player.number_of_wins) }
                        <br />
                        { format!("Losses: {}", player.number_of_losses) }
                        <br />
                        { format!("Ties: {}", player.number_of_ties) }
                    </p>
                </div>
            </div>
        }
    }
}
