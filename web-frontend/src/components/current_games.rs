use bad_api_types::{Game, clean_ws_string};
use futures::StreamExt;
use reqwasm::websocket::{futures::WebSocket, Message};
use ringbuffer::{ConstGenericRingBuffer, RingBufferExt, RingBufferRead, RingBufferWrite};
use yew::prelude::*;

use crate::components::game_card::GameCard;

const NUM_GAMES: usize = 16;
pub enum Msg {
    Connect,
    SetWsState(WsState),
    ReceiveWsData(Game),
    Ignore,
}

#[derive(Debug)]
enum WsState {
    Disconnected,
    Connecting,
    Connected,
    Failed,
}
pub struct CurrentGames {
    games: ConstGenericRingBuffer<Game, NUM_GAMES>,
    ws: WsState,
}

impl Component for CurrentGames {
    type Message = Msg;
    type Properties = ();

    fn create(ctx: &Context<Self>) -> Self {
        ctx.link().send_message(Msg::Connect);
        Self {
            ws: WsState::Disconnected,
            games: ConstGenericRingBuffer::<_, NUM_GAMES>::new(),
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::Connect => {
                let ws_got_data = ctx.link().callback(|game: Game| Msg::ReceiveWsData(game));
                let ws_got_error = ctx
                    .link()
                    .callback(|_: String| Msg::SetWsState(WsState::Disconnected));
                    ctx.link().send_future(async move {
                    if let Ok(ws) = WebSocket::open("ws://bad-api-assignment.reaktor.com/rps/live")
                    {
                        let (_, mut read) = ws.split();
                        loop {
                            let next = read.next().await;
                            log::info!("{:?}", next);
                            if let Some(Ok(msg)) = next {
                                match msg {
                                    Message::Text(txt) => {
                                        let msg_str: String = clean_ws_string(txt);
                                        // Ignore messages that failed to parse
                                        if let Ok(game) = serde_json::from_str::<Game>(&msg_str) {
                                            ws_got_data.emit(game);
                                        }
                                    }
                                    _ => return Msg::SetWsState(WsState::Disconnected),
                                }
                            }
                        }
                    }
                    Msg::SetWsState(WsState::Disconnected)
                });

                ctx.link().send_message(Msg::SetWsState(WsState::Connected));
                false
            }
            Msg::SetWsState(ws_state) => {
                log::info!("{:?}", ws_state);
                self.ws = ws_state;
                true
            }
            Msg::ReceiveWsData(game) => {
                match game {
                    Game::Result(res) => {
                        let find_game = self
                            .games
                            .iter()
                            .enumerate()
                            .find(|(_, g)| g.game_id() == &res.game_id);
                        if let Some((index, game)) = find_game {
                            self.games[index as isize] = Game::Result(res);
                        }
                    }
                    Game::Begin(new_game) => {
                        // Don't show self play / practice games
                        if new_game.player_a == new_game.player_b {
                            return false;
                        }
                        self.games.push(Game::Begin(new_game));
                    }
                }
                true
            }
            _ => false,
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();
        html! {
            <div class="message live">
                { match self.ws {
                    WsState::Connected => {
                        html!{<p class="message-header"> {"Live games"} </p>}
                    },
                    _ => {
                        html!{
                            <>
                            <span> { "Ws disconnected" } </span>
                            <button onclick={link.callback(|_| Msg::Connect)} > { "Try connect" }</button>
                            </>
                        }
                    }
                  }
                }
            { self.game_list_view() }
            </div>
        }
    }
}

impl CurrentGames {
    fn game_list_view(&self) -> Html {
        html! {
            <ul>
        <div class="message-body">
            {for self.games.iter().rev().map(|g| {
                html!{<li> <GameCard game= {g.clone()} /> </li>}
            })}
        </div>

            </ul>
        }
    }
}
