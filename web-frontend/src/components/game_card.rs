use crate::Route;
use bad_api_types::Game;
use std::time;
use yew::prelude::*;
use yew_router::prelude::*;

#[derive(Clone, Debug, PartialEq, Properties)]
pub struct Props {
    pub game: Game,
}
pub struct GameCard;

impl Component for GameCard {
    type Message = ();
    type Properties = Props;

    fn create(ctx: &Context<Self>) -> Self {
        Self
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let game = &ctx.props().game;

        match game {
            Game::Result(game) => html! {
                <div class="card game_end">
                    <div class="card-content">
                        <h5><b> { format!("{}", game.game_id) } </b></h5>
                        <h7>{ format!("{:?}", chrono::NaiveDateTime::from_timestamp(game.time/1000, (game.time % 1000) as u32)) }</h7>
                        <p>
                            <Link<Route> to={Route::Player {name: game.player_a.clone()}}>
                                { format!("{}", game.player_a) }
                            </Link<Route>> <span> {" played : "} </span>
                            <span> { format!("{:?}", game.hand_a) }</span>
                            <br />

                            <Link<Route> to={Route::Player {name: game.player_b.clone()}}>
                                { format!("{}", game.player_b) }
                            </Link<Route>> <span> {" played : "} </span>
                            <span> { format!("{:?}", game.hand_b) }</span>
                        </p>
                    </div>
                </div>
            },
            Game::Begin(game) => html! {
                <div class="card game_start">
                    <div class="card-content">
                        <h5><b> { format!("{}", game.game_id) } </b></h5>
                        <p>
                            <span> { format!("{}", game.player_a) }</span>
                            <br />
                            <span> { format!("{}", game.player_b) }</span>
                        </p>
                    </div>
                </div>
            },
        }
    }
}
