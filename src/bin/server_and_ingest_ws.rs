use bad_api_ingest::ingest_live_games;
use web_backend::run_web_server;

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    dotenv::dotenv().ok();
    let conn_str =
        std::env::var("DATABASE_URL").expect("Env var DATABASE_URL is required for this example.");
    let pool = sqlx::PgPool::connect(&conn_str)
        .await
        .expect("Unable to acquire db connection");

    let server_task = tokio::task::spawn(run_web_server());
    loop {
        let websocket_url = std::env::var("WS_URL").expect("Env var WS_URL must be set");

        tracing::info!("Starting live game ingestion");
        match ingest_live_games(&websocket_url, pool.clone()).await {
            Ok(()) => {
                tracing::info!("Live game ingestion ended with Ok(())")
            }
            Err(e) => {
                tracing::error!("Live game ingestion ended with error: {}", e);
            }
        }
    }
    server_task.await;
}
