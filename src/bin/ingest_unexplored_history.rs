#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    dotenv::dotenv().ok();

    let conn_str =
        std::env::var("DATABASE_URL").expect("Env var DATABASE_URL is required for this example.");
    let pool = sqlx::PgPool::connect(&conn_str)
        .await
        .expect("Unable to acquire db connection");

    let base_url = "https://bad-api-assignment.reaktor.com";
    let start_page = "/rps/history";
    bad_api_ingest::crawl_unexplored_history(base_url, start_page, &pool).await;
}
