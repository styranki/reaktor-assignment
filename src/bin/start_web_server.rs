use web_backend::run_web_server;
#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    dotenv::dotenv().ok();
    run_web_server().await;
}
