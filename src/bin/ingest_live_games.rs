#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    dotenv::dotenv().ok();

    let conn_str =
        std::env::var("DATABASE_URL").expect("Env var DATABASE_URL is required for this example.");
    let pool = sqlx::PgPool::connect(&conn_str)
        .await
        .expect("Unable to acquire db connection");
    let ws_url = "ws://bad-api-assignment.reaktor.com/rps/live";

    loop {
        tracing::info!("Starting live game ingestion");
        let ws_result = bad_api_ingest::ingest_live_games(ws_url, pool.clone()).await;
        tracing::info!("Live game ingestion ended");
        tracing::info!("{:?}", ws_result);
    }
}
