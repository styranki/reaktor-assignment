-- Add up migration script here
CREATE TYPE hand AS ENUM ('ROCK', 'PAPER', 'SCISSORS');

CREATE TABLE players (
	name VARCHAR PRIMARY KEY,
	number_of_games INT DEFAULT 0 NOT NULL,
	number_of_wins INT DEFAULT 0 NOT NULL,
	number_of_ties INT DEFAULT 0 NOT NULL,
	number_of_losses INT DEFAULT 0 NOT NULL,
	number_of_rocks INT DEFAULT 0 NOT NULL,
	number_of_papers INT DEFAULT 0 NOT NULL,
	number_of_scissors INT DEFAULT 0 NOT NULL
);

CREATE TABLE games (
	game_id VARCHAR PRIMARY KEY,
	time BIGINT NOT NULL,
	player_a VARCHAR references players(name) NOT NULL,
	player_b VARCHAR references players(name) NOT NULL,
	hand_a hand NOT NULL,
	hand_b hand NOT NULL
);
CREATE INDEX idx_games_player_a ON games(player_a);
CREATE INDEX idx_games_player_b ON games(player_b);
CREATE INDEX idx_games_time ON games(time);

CREATE TABLE visited_pages (
	cursor VARCHAR PRIMARY KEY
);
