/*! This library contains async helper functions that crawl bad-api and ingest the data into database

    FIXED
   ~~Currently the error types for functions are [`Box<dyn std::error::Error>`] and [`IngestError`], which is not ideal:~~
    ~~* Libraries should **really** return concrete error types instead of trait objects~~
    ~~* [`IngestError`]  doesn't implement [`std::error::Error`] which is the standard error trait~~
*/

use serde::Deserialize;
use sqlx::PgPool;
use std::time::SystemTime;

use futures_util::StreamExt;
use thiserror::Error;
use tokio::time;
use tokio_tungstenite::connect_async;

use bad_api_types::{Game, clean_ws_string};
use database_abstraction::{insert_game, insert_visited_page};

// Helper type for deserializing history pages
#[derive(Debug, Deserialize)]
struct BadApiHistoryPage {
    #[serde(default)]
    cursor: Option<String>,
    data: Vec<Game>,
}

/// Something went wrong while ingesting data
///
/// NOTE: implement Error. Maybe using thiserror
#[derive(Debug, Error)]
pub enum IngestError {
    #[error("{0}")]
    DbError(sqlx::error::Error),
    #[error("{0}")]
    ReqwestError(reqwest::Error),
    #[error("{0}")]
    TokioError(tokio::task::JoinError),
    #[error("Websocket disconnected")]
    WebSocketError,
    #[error("{0}")]
    SerdeError(serde_json::Error),
}

/// Ingest live games from websocket into database
///
/// Only accepts [`GameResult`]. Games in progress are discarded
///
/// FIXME: handle websocket errors
pub async fn ingest_live_games(websocket_url: &str, pool: PgPool) -> Result<(), IngestError> {
    let (ws_stream, _) = connect_async(websocket_url)
        .await
        .map_err(|_| IngestError::WebSocketError)?;
    let (_write, mut read) = ws_stream.split();
    tracing::info!("Connected to websocket: \"{}\"", websocket_url);

    loop {
        let next = read.next().await;
        match next {
            Some(Ok(result)) => {
                if !result.is_text() {
                    continue;
                }
                // Unwrap is safe bacause of the if statement above
                ingest_ws_text(result.to_text().unwrap().to_string(), pool.clone()).await;
            }
            // FIXME: Find out what it mean for .next() to return None. usually in rust iters = last value?
            _ => {
                tracing::error!("WS disconnected");
                return Err(IngestError::WebSocketError);
            }
        }
    }
}

/// Ingest a single websocket message to the db
async fn ingest_ws_text(s: String, pool: PgPool) -> Result<(), IngestError> {
    let msg_str = clean_ws_string(s);
    match serde_json::from_str::<Game>(&msg_str) {
        Ok(game) => {
            if let Game::Result(g) = game {
                let insert_res = insert_game(g, pool).await;

                if let Err(e) = insert_res {
                    tracing::warn!("Failed to insert live game to db");
                    tracing::warn!("{}", e);
                    return Err(IngestError::DbError(e));
                }
            }

            return Ok(());
        }
        Err(e) => {
            tracing::warn!("Deserialization failed: {}", e);
            return Err(IngestError::SerdeError(e));
        }
    }
}

/// Crawls bad-api history pages until it reaches the last page of history
///
/// Return [`Ok(())`] if last page was reached, [`IngestError`] otherwise
pub async fn crawl_entire_history(
    base_url: &str,
    start_page: &str,
    pool: &PgPool,
) -> Result<(), IngestError> {
    let client = reqwest::Client::new();
    let mut page = start_page.to_string();

    loop {
        tracing::debug!("Ingesting page: {}", page);
        match ingest_history_page(base_url, &page, &client, pool).await {
            Ok((next_page, _)) => match next_page {
                Some(p) => page = p.to_string(),
                None => {
                    tracing::info!("Reached last page: {}", page);
                    return Ok(());
                }
            },
            Err(e) => {
                tracing::error!("{:?}", e);
                return Err(e);
            }
        }
    }
}

/// Crawls bad-api history pages until it reaches an already visited page or the last page of history
///
/// Return [`Ok(())`] if already explored or last page was reached, [`IngestError`] otherwise
pub async fn crawl_unexplored_history(
    base_url: &str,
    start_page: &str,
    pool: &PgPool,
) -> Result<(), IngestError> {
    let client = reqwest::Client::new();
    let mut page = start_page.to_string();

    loop {
        tracing::debug!("Ingesting page: {}", page);
        match ingest_history_page(base_url, &page, &client, pool).await {
            Ok((next_page, already_visited)) => {
                if already_visited {
                    tracing::info!("Reached already visited page: {}. Exiting", page);
                    return Ok(());
                }
                match next_page {
                    Some(p) => page = p.to_string(),
                    None => {
                        tracing::info!("Reached last page: {}", page);
                        return Ok(());
                    }
                }
            }
            Err(e) => {
                tracing::error!("{:?}", e);
                return Err(e);
            }
        }
    }
}

/// Handle ratelimit
/// Try to map headers into u64
fn calc_sleep_duration_seconds(headers: &reqwest::header::HeaderMap) -> Option<time::Duration> {
    match (
        headers
            .get("x-ratelimit-remaining")
            .and_then(|a| a.to_str().ok())
            .and_then(|a| a.parse::<u64>().ok()),
        headers
            .get("x-ratelimit-reset")
            .and_then(|a| a.to_str().ok())
            .and_then(|a| a.parse::<u64>().ok()),
    ) {
        (Some(remaining), Some(reset)) => {
            if remaining < 1 {
                // #FIXME: Potential panic is system time < unix epoch
                // Unlikely but could happen on misconfigured systems
                let cur_time = SystemTime::now()
                    .duration_since(SystemTime::UNIX_EPOCH)
                    .unwrap();
                let wait_duration =
                    time::Duration::from_secs(1 + reset.saturating_sub(cur_time.as_secs()));
                return Some(wait_duration);
            }
            None
        }
        // If either of the headers is missing return None
        _ => None,
    }
}

/// Requests a page from bad-api and ingests it into database
///
/// Will sleep if rate limit reached
///
/// Returns tuple containing next page and  bool representing if page is already in the db
async fn ingest_history_page(
    base_url: &str,
    page: &str,
    client: &reqwest::Client,
    pool: &PgPool,
) -> Result<(Option<String>, bool), IngestError> {
    tracing::debug!("Current cursor {}", page);
    let request_start = time::Instant::now();
    let page_resp = client
        .get(format!("{}{}", base_url, page))
        .send()
        .await
        .map_err(|e| IngestError::ReqwestError(e))?;

    let headers = page_resp.headers();

    if let Some(wait_duration) = calc_sleep_duration_seconds(headers) {
        tracing::info!(
            "Rate limit reached sleeping for {} seconds",
            wait_duration.as_secs()
        );
        time::sleep(wait_duration).await;
        tracing::info!("Sleep done");
    }

    let history_page = page_resp
        .json::<BadApiHistoryPage>()
        .await
        .map_err(|e| IngestError::ReqwestError(e))?;
    let next_page = history_page.cursor.clone();

    // Ingest games before inserting the visited page, so that it's not possible
    // for a page to be marked "visited" if all its games aren't in the db
    ingest_games(history_page.data, pool.clone()).await?;
    let page_db_result = insert_visited_page(page.to_string(), pool.clone()).await;

    tracing::info!(
        "Ingested page {} in {} ms",
        page,
        time::Instant::now()
            .duration_since(request_start)
            .as_millis()
    );

    match page_db_result {
        Ok(_) => Ok((next_page, false)),
        Err(sqlx::Error::Database(e)) => {
            // FIXME: Make sure that the database error resulted from the page already existing in the db
            // Currently no validation
            tracing::warn!("{}", e);
            Ok((next_page, true))
        }
        Err(e) => Err(IngestError::DbError(e)),
    }
}

/// Helper function that spawns async task for each game and tries to insert it into db
async fn ingest_games(games: Vec<Game>, pool: PgPool) -> Result<(), IngestError> {
    // Insert games
    let num_games = games.len();
    let db_start = time::Instant::now();
    let mut handles: Vec<_> = Vec::new();
    for game in games {
        match game {
            Game::Begin(_) => {}
            Game::Result(result) => {
                let handle = tokio::spawn(insert_game(result, pool.clone()));
                handles.push(handle);
            }
        }
    }

    for handle in handles {
        let status = handle.await.map_err(|e| IngestError::TokioError(e))?;
        match status {
            Ok(_) => {}
            // #FIXME Decide what to do with different error types
            // Don't exit on single player rps
            Err(sqlx::Error::Protocol(s)) => tracing::warn!("Protocol error: {}", s),
            // Also ignore errors from trying to items already existing
            // Should the function not return on some other error type?
            Err(sqlx::Error::Database(s)) => tracing::warn!("Database error: {}", s),
            Err(e) => return Err(IngestError::DbError(e)),
        }
    }

    tracing::info!(
        "Handled {} records in {} ms",
        num_games,
        time::Instant::now().duration_since(db_start).as_millis()
    );

    Ok(())
}

#[cfg(test)]
mod tests {
    use crate::calc_sleep_duration_seconds;
    use reqwest::header::{HeaderMap, HeaderName, HeaderValue};

    #[test]
    fn test_missing_headers() {
        assert_eq!(
            calc_sleep_duration_seconds(&reqwest::header::HeaderMap::new()),
            None
        )
    }

    #[test]
    fn test_missing_ratelimit_reset() {
        let mut headers = HeaderMap::new();
        headers.append(
            HeaderName::from_static("x-ratelimit-reset"),
            HeaderValue::from_static("1"),
        );
        assert!(calc_sleep_duration_seconds(&headers).is_none());
    }

    #[test]
    fn test_missing_ratelimit_remaining() {
        let mut headers = HeaderMap::new();
        headers.append(
            HeaderName::from_static("x-ratelimit-ratelimit"),
            HeaderValue::from_static("1"),
        );
        assert!(calc_sleep_duration_seconds(&headers).is_none());
    }

    #[test]
    fn test_correct_headers() {
        // Max u64 shouldn't fail anytime soon :-)
        let max_u64_str = format!("{}", std::u64::MAX);
        let mut headers = HeaderMap::new();
        headers.append(
            HeaderName::from_static("x-ratelimit-remaining"),
            HeaderValue::from_static("0"),
        );
        headers.append(
            HeaderName::from_static("x-ratelimit-reset"),
            HeaderValue::from_str(&max_u64_str).unwrap(),
        );
        assert!(calc_sleep_duration_seconds(&headers).is_some())
    }

    #[test]
    fn test_correct_headers_wrong_header_value() {
        // Max u64 shouldn't fail anytime soon :-)
        let max_u64_str = format!("{}", std::u64::MAX);
        let mut headers = HeaderMap::new();
        headers.append(
            HeaderName::from_static("x-ratelimit-remaining"),
            HeaderValue::from_static("juolupukin_reki"),
        );
        headers.append(
            HeaderName::from_static("x-ratelimit-reset"),
            HeaderValue::from_str(&max_u64_str).unwrap(),
        );
        assert!(calc_sleep_duration_seconds(&headers).is_none())
    }
}
