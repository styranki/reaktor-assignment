use axum::{
    extract::{Extension, Path, Query},
    handler::Handler,
    http::{Method, StatusCode, Uri},
    response::IntoResponse,
    routing::{get, get_service},
    AddExtensionLayer, Json, Router,
};
use std::{net::SocketAddr, str::FromStr};
use tower::ServiceBuilder;
use tower_http::{
    cors::{CorsLayer, Origin},
    services::fs::{ServeDir, ServeFile},
    trace::TraceLayer,
};

use bad_api_types::{GameResult, Hand, Player};
use database_abstraction as db;

pub async fn run_web_server() {
    dotenv::dotenv().ok();

    tracing::info!("Starting web server");
    let conn_str =
        std::env::var("DATABASE_URL").expect("Env var DATABASE_URL is required for this example.");
    let pool = sqlx::PgPool::connect(&conn_str)
        .await
        .expect("Unable to acquire db connection");
    let db_pool = pool; // Arc::new(pool);
    let api_routes = Router::new()
        .route("/", get(root))
        .route("/players", get(get_players))
        .route("/players/count", get(get_number_of_players))
        .route("/player/:name", get(get_player))
        .route("/player/:name/games", get(get_player_games))
        .route("/games/recent", get(get_recent_games))
        .route("/games/count", get(get_number_of_games));

    let app = Router::new()
        .nest("/api", api_routes)
        .route(
            "/",
            get_service(ServeFile::new("./web-frontend/dist/index.html")).handle_error(
                |error: std::io::Error| async move {
                    (
                        StatusCode::INTERNAL_SERVER_ERROR,
                        tracing::error!("Unhandled internal error: {}", error),
                    )
                },
            ),
        )
        .route(
            "/:file",
            get_service(ServeDir::new("./web-frontend/dist/")).handle_error(
                |error: std::io::Error| async move {
                    (
                        StatusCode::INTERNAL_SERVER_ERROR,
                        tracing::error!("Unhandled internal error: {}", error),
                    )
                },
            ),
        )
        .layer({
            let trace_layer = TraceLayer::new_for_http();
            let cors_permissive = CorsLayer::permissive();
            ServiceBuilder::new()
                .layer(trace_layer)
                .layer(cors_permissive)
                .layer(AddExtensionLayer::new(db_pool))
        });
    let app = app.fallback(fallback.into_service());
    let addr_str = std::env::var("HOST_ADDRESS").expect("Env var HOST_ADDRESS must be set");
    let port = std::env::var("PORT").expect("Env var PORT must be set");
    let addr = SocketAddr::from_str(format!("{}:{}", &addr_str, &port).as_str())
        .expect("Invalid address format");
    tracing::info!("Listening on {}", addr);

    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

#[derive(serde::Deserialize)]
struct Pagination {
    offset: usize,
    limit: usize,
}

impl Default for Pagination {
    fn default() -> Self {
        Self {
            offset: 1,
            limit: 30,
        }
    }
}

async fn root(Extension(db_pool): Extension<sqlx::PgPool>) -> impl IntoResponse {
    tracing::debug!("{:?}", db_pool);
    (StatusCode::OK, "root")
}

async fn fallback(uri: Uri) -> impl IntoResponse {
    (StatusCode::NOT_FOUND, format!("No route for: {}", uri))
}

async fn get_player(
    Path(name): Path<String>,
    Extension(db_pool): Extension<sqlx::PgPool>,
) -> Result<impl IntoResponse, StatusCode> {
    match db::get_player(name, db_pool.clone()).await {
        Ok(player) => Ok((StatusCode::OK, Json(player))),
        Err(e) => match e {
            sqlx::Error::RowNotFound => Err(StatusCode::NOT_FOUND),
            _ => Err(StatusCode::INTERNAL_SERVER_ERROR),
        },
    }
}

async fn get_players(
    Extension(db_pool): Extension<sqlx::PgPool>,
    pagination: Option<Query<Pagination>>,
) -> Result<impl IntoResponse, StatusCode> {
    match db::get_players(
        pagination.as_ref().map(|x| x.offset as u64),
        pagination.as_ref().map(|x| x.limit as u64),
        db_pool.clone(),
    )
    .await
    {
        Ok(players) => Ok((StatusCode::OK, Json(players))),
        Err(e) => match e {
            sqlx::Error::RowNotFound => Err(StatusCode::NOT_FOUND),
            _ => Err(StatusCode::INTERNAL_SERVER_ERROR),
        },
    }
}

async fn get_player_games(
    Path(name): Path<String>,
    pagination: Option<Query<Pagination>>,
    Extension(db_pool): Extension<sqlx::PgPool>,
) -> Result<impl IntoResponse, StatusCode> {
    match db::get_player_games(
        name,
        pagination.as_ref().map(|x| x.offset as u64),
        pagination.as_ref().map(|x| x.limit as u64),
        db_pool.clone(),
    )
    .await
    {
        Ok(games) => Ok((StatusCode::OK, Json(games))),
        Err(e) => match e {
            sqlx::Error::RowNotFound => Err(StatusCode::NOT_FOUND),
            _ => Err(StatusCode::INTERNAL_SERVER_ERROR),
        },
    }
}

async fn get_recent_games(
    Extension(db_pool): Extension<sqlx::PgPool>,
    pagination: Option<Query<Pagination>>,
) -> Result<impl IntoResponse, StatusCode> {
    match db::get_most_recent_games(
        pagination.as_ref().map(|x| x.offset as u64),
        pagination.as_ref().map(|x| x.limit as u64),
        db_pool.clone(),
    )
    .await
    {
        Ok(games) => Ok((StatusCode::OK, Json(games))),
        Err(e) => match e {
            sqlx::Error::RowNotFound => Err(StatusCode::NOT_FOUND),
            _ => Err(StatusCode::INTERNAL_SERVER_ERROR),
        },
    }
}
async fn get_number_of_games(
    Extension(db_pool): Extension<sqlx::PgPool>,
) -> Result<impl IntoResponse, StatusCode> {
    match db::get_number_of_games(db_pool.clone()).await {
        Ok(num) => Ok((StatusCode::OK, format!("{}", num))),
        Err(e) => match e {
            sqlx::Error::RowNotFound => Err(StatusCode::NOT_FOUND),
            _ => Err(StatusCode::INTERNAL_SERVER_ERROR),
        },
    }
}
async fn get_number_of_players(
    Extension(db_pool): Extension<sqlx::PgPool>,
) -> Result<impl IntoResponse, StatusCode> {
    match db::get_number_of_players(db_pool.clone()).await {
        Ok(num) => Ok((StatusCode::OK, format!("{}", num))),
        Err(e) => match e {
            sqlx::Error::RowNotFound => Err(StatusCode::NOT_FOUND),
            _ => Err(StatusCode::INTERNAL_SERVER_ERROR),
        },
    }
}
